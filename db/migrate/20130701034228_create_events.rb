class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.datetime :date_start
      t.datetime :date_end
      t.references :category, index: true
      t.references :city, index: true
      t.string :location
      t.references :user, index: true

      t.timestamps
    end
  end
end
