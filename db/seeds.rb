# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Country.delete_all
mx = Country.create(name:'Mexico')

State.delete_all
yuc = State.create(name:'Yucatan',country:mx)
qr = State.create(name:'Quintana Roo',country:mx)

City.delete_all
mid = City.create(name:'Merida', state:yuc)
ctm = City.create(name:'Chetumal', state:qr)
cun = City.create(name:'Cancun', state:qr)

Category.delete_all
cat_social = Category.create(:name=>'Social')
cat_cultural = Category.create(:name=>'Cultural')
cat_deportiva = Category.create(:name=>'Deportiva')
cat_musical = Category.create(:name=>'Musical')
cat_infantil = Category.create(:name=>'Infantil')

Event.delete_all
#Event.create(:name=>'Carrera por la paz', :date=>DateTime.new(2013, 05, 30, 10, 00, 00), :city=>mid,
#	:category=>cat_deportiva, :location=>'Carretera a Merida-Progreso',
#	:description=>'Carrera de 10 kms por la paz a celebrarse en la Carrera Merida-Progreso')
#
#Event.create(:name=>'Concierto EXA', :date=>DateTime.new(2013, 05, 30, 16, 30, 00), :city=>mid,
#	:category=>cat_musical, :location=>'Estacionamiento de Plaza Sendero',
#	:description=>'Concierto anual de EXA en el estacionamiento de la Plaza Sendero')
#
#Event.create(:name=>'Danza infantil', :date=>DateTime.new(2013, 06, 01, 19, 00, 00), :city=>mid,
#	:category=>cat_cultural, :location=>'Plaza Galerias',
#	:description=>'Ven y disfruta con tu familia de este bello espectaculo de danza de pequenitas de 6 a 10 anos')
#
#Event.create(:name=>'Espectaculo infantil de marionetas', :date=>DateTime.new(2013, 06, 02, 11, 00, 00), :city=>mid,
#	:category=>cat_infantil, :location=>'Monumento a la Patria',
#	:description=>'Espectaculo con marionetas para todos los ninos')
#
#Event.create(:name=>'Carrera de tortugas', :date=>DateTime.new(2013, 06, 16, 10, 00, 00), :city=>mid,
#	:category=>cat_social, :location=>'Parque Aleman',
#	:description=>'Disfruta este dia del padre con una carrera muy peculiar')
#
#Event.create(:name=>'Concierto de Yuri', :date=>DateTime.new(2013, 05, 10, 21, 00, 00), :city=>mid,
#	:category=>cat_musical, :location=>'Centro de Convenciones Siglo XXI',
#	:description=>'Concierto de Yuri')
