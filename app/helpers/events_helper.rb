module EventsHelper

  def link_to_follow(event)

    html = ''

    if event.is_followed_by(current_user)
      html << link_to('Seguir', follow_event_path(event), method: :post, id: 'lnkFollowEvent', class: 'hidden-link')
      html << link_to('Dejar de seguir', unfollow_event_path(event), method: :post, id: 'lnkUnfollowEvent', class: 'visible-link')
    else
      html << link_to('Seguir', follow_event_path(event), method: :post, id: 'lnkFollowEvent', class: 'visible-link')
      html << link_to('Dejar de seguir', unfollow_event_path(event), method: :post, id: 'lnkUnfollowEvent', class: 'hidden-link')
    end
    return raw(html)
  end

end
