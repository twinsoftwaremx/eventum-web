module UsersHelper

  def my_city(user)
    "#{user.city.name}, #{user.city.state.name}" unless user.city.nil?
  end

end
