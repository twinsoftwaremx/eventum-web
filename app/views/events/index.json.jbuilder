json.array!(@events) do |event|
  json.extract! event, :name, :description, :date, :category_id, :city_id, :location, :user_id
  json.url event_url(event, format: :json)
end