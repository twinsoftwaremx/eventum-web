json.array!(@countries) do |country|
  json.extract! country, :name, :language
  json.url country_url(country, format: :json)
end