# == Schema Information
#
# Table name: events
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  date_start  :datetime
#  date_end    :datetime
#  category_id :integer
#  city_id     :integer
#  location    :string(255)
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Event < ActiveRecord::Base
  belongs_to :category
  belongs_to :city
  belongs_to :user
  has_and_belongs_to_many :followers, class_name: 'User'

  resourcify

  mount_uploader :image, ImageUploader

  def date
    if date_start.yday == date_end.yday && date_start.year == date_end.year
      "#{I18n.localize(date_start)} - #{date_end.hour}:#{date_end.min.to_s.rjust(2, '0')}"
    else
      "#{I18n.localize(date_start)} - #{I18n.localize(date_end)}"
    end
  end

  def city_state
    "#{city.name}, #{city.state.name}" unless city.nil? or city.state.nil?
  end

  def is_followed_by(user)
    followers.exists? (user.id)
  end

  def self.recents(count)
    Event.order(:date_start).limit(count)
  end

  def self.recents_from_city(city_name)
    Event.joins(:city).where(cities: {name: city_name})
  end

  def self.recents_of_category(category_name)
    Event.joins(:category).where(categories: {name: category_name})
  end

  def self.not_followed_by(user)
    @event_ids = Event.includes(:followers).where("events_users.user_id = #{user.id}").select(:id)
    if @event_ids.count == 0
      @event_ids = 0
    end

    Event.includes(:followers)
      .where("(events_users.user_id <> #{user.id} or events_users.user_id is null)")
      .where("(events_users.event_id not in (?)) or events_users.user_id is null", @event_ids)
      .references('events_users')
  end

  def self.coming(count=5)
  	Event.where('date_start > ?', Time.now).order(:date_start).limit(count)
  end

  def self.published_by(user)
    Event.where(user: user)
  end

  def self.followed_by(user)
    user.events
  end

  def self.events_in_city_of(user, count = 5)
    Event.not_followed_by(user).where(city_id: user.city.id).order(:date_start).limit(count)
  end

end
