# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  state_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class City < ActiveRecord::Base
  belongs_to :state
  has_many :events

  def self.with_recent_events(count=5)
    City.joins(:events).select('cities.name, count(city_id) as "event_count"').group('cities.name').order(' event_count desc')
  end
end
