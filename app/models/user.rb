# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  username               :string(255)
#  city_id                :integer
#

class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  #after_create :assign_default_role
  belongs_to :city
  has_and_belongs_to_many :events

  def assign_role(role)
    if role == '1' then
      puts "****************** role: #{role} entered in role 1 *************************"
      add_role(:member)
    elsif role == '2' then
      puts "****************** role: #{role} entered in role 2 *************************"
      add_role(:publisher)
    end
  end

  private
  def assign_default_role
    add_role(:member)
  end

end
