class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new # guest user (not logged in)
    if user.has_role? :member
      can :read, Event
      can :follow, Event
    elsif user.has_role? :publisher
      can :read, :all
      can :create, Event
      can [:update, :destroy], Event, :user_id => user.id
    else
      can :read, Event
    end
  end
end
