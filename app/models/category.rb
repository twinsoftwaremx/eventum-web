# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Category < ActiveRecord::Base
  has_many :events

  def self.with_recent_events(count=5)
    Category.joins(:events).select('categories.name, count(category_id) as "event_count"')
      .group('categories.name').order(' event_count desc')
  end

end
