class Users::RegistrationsController < Devise::RegistrationsController

  def new
    @countries = Country.all
    @states = State.none
    @cities = City.none
    super
  end

  def create
    @countries = Country.all
    @states = State.none
    @cities = City.none
    super

    user_type = params[:user_type]
    resource.assign_role(user_type)
  end

  protected

  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation, :username, :city_id)
  end

end
