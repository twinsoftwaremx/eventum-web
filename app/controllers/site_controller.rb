class SiteController < ApplicationController
  def index

    @city_param = params[:city]
    @category_param = params[:category]

    if not @city_param.nil?
      @events = Event.recents_from_city(@city_param)
    elsif not @category_param.nil?
      @events = Event.recents_of_category(@category_param)      
    else
      if current_user
        @events = Event.events_in_city_of(current_user)
      else
        @events = Event.recents(10)
      end
    end

    @cities_with_recent_events = City.with_recent_events
    @categories_with_recent_events = Category.with_recent_events

  end
end
