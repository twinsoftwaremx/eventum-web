class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy, :follow, :unfollow]
  before_action :fill_tables, only: [:new, :create, :edit, :update]
  before_action :authenticate_user!, except: [:show]

  # GET /events
  # GET /events.json
  def index
    if current_user.has_role? :publisher
      @events = Event.published_by current_user
      index_publisher
    elsif current_user.has_role? :member
      @events = Event.followed_by current_user
      @events_in_city = Event.events_in_city_of current_user
      index_member
    end
  end

  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)

    date_start = Time.zone.parse(event_params[:date_start])
    date_end = Time.zone.parse(event_params[:date_end])

    @event.date_start = date_start
    @event.date_end = date_end
    if params[:city_id].nil? then
      @event.city = current_user.city
    else
      @event.city = City.find(params[:city_id])
    end
    @event.user = current_user

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render action: 'show', status: :created, location: @event }
      else
        format.html { render action: 'new' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
    end
  end

  def follow
    if not @event.is_followed_by(current_user)
      @event.followers << current_user
      respond_to do |format|
        format.json { head :no_content }
      end
    end
  end

  def unfollow
    if @event.is_followed_by(current_user)
      @event.followers.delete(current_user)
      respond_to do |format|
        format.json { head :no_content }
      end
    end
  end

  protected
  def index_publisher
    render(template: 'events/index_publisher')
  end

  def index_member
    render(template: 'events/index_member')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    def fill_tables
      @categories = Category.all
      @countries = Country.all
      @states = State.none
      @cities = City.none
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :description, :date_start, :date_end, :category_id, :city_id, :location, :user_id, :image)
    end
end
