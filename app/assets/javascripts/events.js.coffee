# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->

  $('#lnkFollowEvent').click (event) ->
    event.preventDefault
    
    url = $(this).attr('href')

    $.ajax url,
      type: 'POST'
      dataType: 'json'
      success: ->
        $('#lnkFollowEvent').removeClass('visible-link')
        $('#lnkFollowEvent').addClass('hidden-link')
        
        $('#lnkUnfollowEvent').removeClass('hidden-link')
        $('#lnkUnfollowEvent').addClass('visible-link')
      error: ->
        alert('error')

    return false

  $('#lnkUnfollowEvent').click (event) ->
    event.preventDefault
    
    url = $(this).attr('href')

    $.ajax url,
      type: 'POST'
      dataType: 'json'
      success: ->
        $('#lnkFollowEvent').removeClass('hidden-link')
        $('#lnkFollowEvent').addClass('visible-link')
        
        $('#lnkUnfollowEvent').removeClass('visible-link')
        $('#lnkUnfollowEvent').addClass('hidden-link')
      error: ->
        alert('error')

    return false