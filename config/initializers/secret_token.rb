# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Eventum::Application.config.secret_key_base = 'e6482ff8ae7629be4a8586cd7826efed0053c4b363b592c41e62ce3e7d651ee29b395da6b62be20516a2019fe10e83a8972fdc0b44d31b6c9079de48d1221c0c'
